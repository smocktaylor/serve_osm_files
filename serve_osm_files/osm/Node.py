from xml.etree import ElementTree as ElementTree

from .LatLon import LatLon
from .OsmPrimitive import OsmPrimitive


class Node(OsmPrimitive):
    """
    Base class for nodes
    """

    def __init__(self, id: int, latlon: LatLon, tags: dict, version: int = 0):
        OsmPrimitive.__init__(self, id, tags, version=version)
        self.latlon = latlon

    def to_xml(self):
        return self.xml_tags(
            ElementTree.Element(
                "node",
                attrib={
                    "id": str(self.id),
                    "version": str(self.version),
                    "lat": self.latlon.latitude,
                    "lon": self.latlon.longitude,
                },
            )
        )
