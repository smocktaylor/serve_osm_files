from xml.etree import ElementTree as ElementTree


class OsmPrimitive(object):
    """
    Base OpenStreetMap primitive class
    """

    def __init__(self, id: int, tags: dict, version: int = 0):
        self.tags = tags
        self.id = id
        self.version = version

    def get_tags(self):
        return self.tags

    def json_tags(self):
        return self.tags

    def xml_tags(self, element):
        for key in self.tags:
            ElementTree.SubElement(
                element, "tag", attrib={"k": key, "v": self.tags[key]}
            )
        return element
