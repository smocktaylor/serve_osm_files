class RelationMember:
    """
    Base class for relation members
    """

    def __init__(self, role, member):
        self.role = role
        self.member = member
