#!/usr/bin/env python3


class BBox(object):
    """
    BBox class
    """

    def __init__(self, bounds_array: list):
        self.lat1 = bounds_array[1]
        self.lon1 = bounds_array[0]
        self.lat2 = bounds_array[3]
        self.lon2 = bounds_array[2]

    def __str__(self) -> str:
        return ",".join([self.lon1, self.lat1, self.lon2, self.lat2])

    def overpass(self) -> str:
        return ",".join([self.lat1, self.lon1, self.lat2, self.lon2])

    def area(self) -> float:
        """
        Return the area (in degrees) of the bbox
        """
        return abs(float(self.lat2) - float(self.lat1)) * abs(
            float(self.lon2) - float(self.lon1)
        )

    def is_empty(self) -> bool:
        """
        Return true if the bbox is effectively empty
        """
        return (
            self.lat1 is None
            and self.lat2 is None
            and self.lon1 is None
            and self.lon2 is None
            or self.area() == 0
        )
