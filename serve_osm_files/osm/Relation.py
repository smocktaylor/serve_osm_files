from .OsmPrimitive import OsmPrimitive


class Relation(OsmPrimitive):
    """
    Base class for relations
    """

    def __init__(self, id, members_with_roles, tags):
        OsmPrimitive.__init__(self, id, tags)
        self.members_with_roles = members_with_roles
        self.tags = tags
