from xml.etree import ElementTree as ElementTree

from .BBox import BBox
from .LatLon import LatLon
from .Node import Node
from .OsmPrimitive import OsmPrimitive
from .Relation import Relation
from .RelationMember import RelationMember
from .Way import Way


class DataSet(object):
    """
    Dataset class
    """

    def __init__(self, bbox: BBox, elements: list):
        self.bbox = bbox
        self.elements = elements
        self.nodes = {}
        self.ways = {}
        self.relations = {}

    def convert_to_xml(self):
        bounds = self.bbox
        osm = ElementTree.Element(
            "osm", attrib={"version": "0.6", "generator": "serve_osm"}
        )
        if bounds:
            ElementTree.SubElement(
                osm,
                "bounds",
                attrib={
                    "minlat": bounds.lat1,
                    "minlon": bounds.lon1,
                    "maxlat": bounds.lat2,
                    "maxlon": bounds.lon2,
                    "origin": "serve_osm",
                },
            )
        for primitive in self.elements:
            osm.append(primitive.to_xml())
        return ElementTree.tostring(osm, xml_declaration=True)

    @staticmethod
    def osm_xml_to_dataset(root):
        elements = []
        ways = []
        relations = []
        bbox = None
        ds = DataSet(bbox, elements)
        for element in root:
            e = None
            if element.tag == "node":
                e = DataSet.element_to_osm_object(element)
                ds.nodes[e.id] = e
            elif element.tag == "way":
                ways.append(element)
            elif element.tag == "relation":
                relations.append(element)
            elif element.tag == "bounds":
                attribs = element.attrib
                ds.bbox = BBox(
                    [
                        attribs["minlon"],
                        attribs["minlat"],
                        attribs["maxlon"],
                        attribs["maxlat"],
                    ]
                )
            if e is not None:
                elements.append(e)
        for way in ways:
            e = DataSet.element_to_osm_object(way, ds=ds)
            elements.append(e)
            ds.ways[e.id] = e
        for relation in relations:
            e = DataSet.element_to_osm_object(relation, ds=ds)
            elements.append(e)
            ds.relations[e.id] = e
        ds.elements = elements
        return ds

    @staticmethod
    def element_to_osm_object(element, ds=None):
        if element.tag == "node":
            node = DataSet.node_to_osm_object(element)
            return node
        elif element.tag == "way":
            way = DataSet.way_to_osm_object(element, ds=ds)
            return way
        elif element.tag == "relation":
            relation = DataSet.relation_to_osm_object(element, ds=ds)
            return relation

    @staticmethod
    def node_to_osm_object(element):
        tags = DataSet.get_tags(element)
        return Node(
            element.attrib["id"],
            LatLon(element.attrib["lat"], element.attrib["lon"]),
            tags,
        )

    @staticmethod
    def way_to_osm_object(way, ds=None):
        tags = DataSet.get_tags(way)
        nodes = DataSet.get_nodes(way, ds)
        return Way(way.attrib["id"], nodes, tags)

    @staticmethod
    def relation_to_osm_object(relation, ds=None):
        tags = DataSet.get_tags(relation)
        members = DataSet.get_members(relation, ds)
        return Relation(relation.attrib["id"], members, tags)

    @staticmethod
    def get_members(relation, ds):
        members = []
        for child in relation:
            if child.tag == "member":
                member = None
                if child.attrib["type"] == "way":
                    member = DataSet.find_primitive_id_in_dataset(
                        ds, child.attrib["ref"], class_type=Way
                    )
                elif child.attrib["type"] == "node":
                    member = DataSet.find_primitive_id_in_dataset(
                        ds, child.attrib["ref"], class_type=Node
                    )
                elif child.attrib["type"] == "Relation":
                    member = DataSet.find_primitive_id_in_dataset(
                        ds, child.attrib["ref"], class_type=Relation
                    )
                members.append(RelationMember(child.attrib["role"], member))
        return members

    @staticmethod
    def get_nodes(way, ds):
        nodes = []
        for child in way:
            if child.tag == "nd":
                nodes.append(
                    DataSet.find_primitive_id_in_dataset(
                        ds, child.attrib["ref"], class_type=Node
                    )
                )
        return nodes

    @staticmethod
    def get_tags(element):
        tags = {}
        for child in element:
            if child.tag == "tag":
                tags[child.attrib["k"]] = child.attrib["v"]
        return tags

    @staticmethod
    def find_primitive_id_in_dataset(ds, id, class_type=OsmPrimitive):
        if class_type is Node:
            return ds.nodes.get(id)
        if class_type is Way:
            return ds.ways.get(id)
        if class_type is Relation:
            return ds.relations.get(id)
