from xml.etree import ElementTree as ElementTree

from .OsmPrimitive import OsmPrimitive


class Way(OsmPrimitive):
    """
    Base class for ways
    """

    def __init__(self, id, nodes, tags):
        OsmPrimitive.__init__(self, id, tags)
        self.nodes = nodes
        self.tags = tags

    def is_closed(self):
        """
        Check if the way is a closed way (possible area, depending upon tags)
        """
        return self.nodes[0] is self.nodes[-1]

    def to_xml(self):
        element = ElementTree.Element(
            "way", attrib={"id": str(self.id), "version": str(self.version)}
        )
        for node in self.nodes:
            ElementTree.SubElement(element, "nd", attrib={"ref": str(node.id)})
        return self.xml_tags(element)
