import math


class LatLon(object):
    """
    Base class for location information
    """

    radius = 6371008.7714  # Earth's radius in m. Wiki has it as the average.

    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude

    def get_distance(self, latitude_longitude: "LatLon") -> float:
        """
        This uses the Haversine formula
        """
        distance_longitude_radians = (
            self.longitude - latitude_longitude.longitude
        )
        distance_latitude_radians = self.latitude - latitude_longitude.latitude
        return (
            LatLon.radius
            * 2
            * math.asin(
                math.sqrt(
                    math.sin(distance_latitude_radians / 2) ** 2
                    + math.cos(distance_latitude_radians)
                    * math.cos(distance_longitude_radians)
                    * math.sin(distance_longitude_radians / 2) ** 2
                )
            )
        )

    def to_wkt(self) -> str:
        return "SRID=4326;POINT({longitude} {latitude})".format(
            longitude=self.longitude, latitude=self.latitude
        )
