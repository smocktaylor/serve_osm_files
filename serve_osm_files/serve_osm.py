#!/usr/bin/env python
import logging
import os
import shutil
import subprocess
import tempfile
import time
import traceback
import xml.etree.ElementTree as ET
from threading import Lock

import defusedxml.ElementTree as ElementTree
import flask
import overpass
import requests

from .db import DBBackend
from .osm import BBox, DataSet

app = flask.Flask(__name__)

# While we do not anticipate OpenStreetMap data being larger than 1 MB, just
# in case, we will assume 16 MB.
app.config["MAX_CONTENT_LENGTH"] = 16 * 1024 * 1024
overpassAPI = overpass.API()
overpassLock = Lock()

APP_PATH = os.path.dirname(os.path.realpath(__file__))

# Point at the file. Don't add '.osm' to the end -- it is added automatically
FILES = [
    (
        os.path.join("Mesa County GIS E911 Addresses", "E911_AddressPoints"),
        "mesaCountyE911",
    ),
    (
        os.path.join(
            "Statewide Aggregate Addresses in Colorado 2019 (Public)",
            "CsadPublic",
        ),
        "coloradoAddresses",
    ),
]
CONFLATION_KEYS = ["addr:housenumber", "addr:street"]

# You need to build osmosis. I used ./gradlew assemble in the directory.
# Use the `.bat` file on Microsoft Windows
OSMOSIS_PATH = os.path.join(
    APP_PATH, "../osmosis", "package", "bin", "osmosis"
)
time_populated = None


@app.route("/")
def landing_page():
    info = [
        "<html>",
        "<h1>Add this webserver to MapWithAI</h1>",
        "<h2>DataSets:</h2>",
    ]
    for FILE in FILES:
        info.append(
            "<a href=./{filename}/map>{filename}</a>".format(filename=FILE[1])
        )
    return "<br/>".join(info)


def conflate(external_data: DataSet, osm_data: DataSet):
    """
    Conflate an external dataset and an osm dataset
    """
    app.logger.debug(time.time())
    if external_data is None:
        return "We need data to conflate", 500
    if osm_data is None and (
        external_data.bbox is None or external_data.bbox.is_empty()
    ):
        app.logger.debug(
            "Cannot continue conflation due to missing osm_data or no bbox "
            + "in external_data"
        )
        app.logger.debug("osm_data: " + str(osm_data))
        app.logger.debug("bbox: " + str(external_data.bbox))
        app.logger.debug(
            "bbox is empty: " + str(external_data.bbox.is_empty())
        )
        return (
            "Either we need osm data to conflate against, "
            + "or the external data needs a bounds",
            500,
        )
    elif osm_data is None:
        osm_data = get_possible_duplicates(external_data.bbox)
    try:
        cleaned_data = remove_duplicates(external_data, osm_data)
        return_xml = cleaned_data.convert_to_xml()
    except Exception as error:
        app.logger.debug(error)
        return_xml = external_data.convert_to_xml()
    app.logger.debug(time.time())
    return return_xml


@app.route("/conflate", methods=["POST"])
def conflate_external_data():
    external_data = None
    osm_data = None
    request = flask.request
    app.logger.debug(time.time())
    if "openstreetmap" in request.files:
        osm_data = request.files["openstreetmap"]
    elif "openstreetmap" in request.form:
        osm_data = request.form["openstreetmap"]

    if isinstance(osm_data, str):
        osm_data = ElementTree.fromstring(osm_data)
    if osm_data is not None:
        osm_data = DataSet.osm_xml_to_dataset(osm_data)

    if "external" in request.files:
        external_data = request.files["external"]
    elif "external" in request.form:
        external_data = request.form["external"]
    if isinstance(external_data, str):
        external_data = ElementTree.fromstring(external_data)
    if external_data is not None:
        external_data = DataSet.osm_xml_to_dataset(external_data)
    app.logger.debug(time.time())
    return conflate(external_data, osm_data)


@app.route("/<dataset>/map", methods=["GET", "POST"])
def get_map_data(dataset):
    if dataset in [i[1] for i in FILES]:
        arg_bbox = flask.request.args.get("bbox")
        if arg_bbox:
            bbox = BBox(arg_bbox.split(","))
            possible_duplicates = get_possible_duplicates(bbox)
            try:
                partial_dataset = DBBackend().get_data(dataset, bbox)
            except Exception as e:
                app.logger.critical(traceback.format_exc())
                app.logger.critical(e)
                partial_dataset = create_osm_file(dataset, bbox)
            return conflate(partial_dataset, possible_duplicates)
        else:
            return "DataSet {} not found".format(dataset), 404
    else:
        return "We need a bbox parameter", 500


@app.route("/populate", methods=["GET", "POST"])
def populate():
    app.logger.error("Starting population of database")
    global time_populated
    failure = False
    for filename in FILES:
        if time_populated is not None and time_populated > os.path.getmtime(
            filename[0]
        ):
            continue
        try:
            import_data(filename[0], filename[1])
        except Exception as e:
            app.logger.error(traceback.format_exc())
            app.logger.critical(e)
            failure = True
    if not failure:
        time_populated = time.time()
        return "Success", 200
    return "Failure", 500


def create_osm_file(dataset: str, bbox: BBox) -> DataSet:
    """
    Create the osm file to be sent to the client
    """
    tempdir = tempfile.gettempdir()
    outfile = os.path.join(tempdir, str(bbox) + "_" + dataset)
    base_cmd = [
        OSMOSIS_PATH,
        "--read-xml",
        "enableDateParsing=no",
        "file=" + os.path.join(APP_PATH, dataset + ".osm"),
        "--bounding-box",
        "left=" + bbox.lon1,
        "bottom=" + bbox.lat1,
        "right=" + bbox.lon2,
        "top=" + bbox.lat2,
        "--write-xml",
        outfile,
    ]
    app.logger.debug(" ".join(base_cmd))
    try:
        output = subprocess.check_output(base_cmd)
    except subprocess.CalledProcessError as error:
        app.logger.error("Adding version")
        app.logger.debug(error)
        add_version(os.path.join(APP_PATH, dataset + ".osm"))
        output = subprocess.check_output(base_cmd)
    logging.info(output)
    add_boundingbox(outfile, bbox)
    return DataSet.osm_xml_to_dataset(ElementTree.parse(outfile).getroot())


def get_possible_duplicates(bbox: BBox) -> DataSet:
    query = "("
    delta = (
        150 / 111139
    )  # meters to degrees approximation. Doesn't work well at boundaries
    delta = 0.005
    real_bbox = BBox(
        [
            str(float(bbox.lon1) - delta),
            str(float(bbox.lat1) - delta),
            str(float(bbox.lon2) + delta),
            str(float(bbox.lat2) + delta),
        ]
    )
    try:
        # raise ValueException("TODO Fix OSM call")
        request = requests.get(
            "https://api.openstreetmap.org/api/0.6/map?bbox={bbox}".format(
                bbox=str(real_bbox)
            )
        )
        request.raise_for_status()
        data = DataSet.osm_xml_to_dataset(ElementTree.fromstring(request.text))
        return data
    except Exception as e:
        app.logger.error(e)
    for i in CONFLATION_KEYS:
        query += """nwr({area})["{key}"];""".format(
            area=real_bbox.overpass(), key=i
        )
    query += ");(._;>;);"
    with overpassLock:
        data = overpassAPI.get(query, verbosity="meta", responseformat="xml")
    return DataSet.osm_xml_to_dataset(ElementTree.fromstring(data))


def remove_duplicates(filename, possible_duplicates):
    if isinstance(filename, DataSet):
        dataset = filename
    else:
        tree = ElementTree.parse(filename)
        root = tree.getroot()
        dataset = DataSet.osm_xml_to_dataset(root)
    not_found = []
    possible_duplicates_tagged = [
        i for i in possible_duplicates.elements if len(i.get_tags()) > 0
    ]
    for i in dataset.elements:
        itags = i.get_tags()
        if len(itags) == 0:
            continue
        source = (
            itags["mapwithai:source"] if "mapwithai:source" in itags else None
        )
        del itags["mapwithai:source"]
        found = False
        for j in possible_duplicates_tagged:
            jtags = j.get_tags()
            if len(jtags) == 0:
                continue
            if itags.items() <= jtags.items():
                found = True
                break
        itags["mapwithai:source"] = source
        if not found:
            not_found.append(i)
    return DataSet(dataset.bbox, not_found)


def import_data(filename: str, datasetname: str):
    directory = os.path.join(APP_PATH, "../datasets")
    if not os.path.isdir(directory):
        os.mkdir(directory)
    filename = os.path.join(APP_PATH, "../datasets", filename + ".osm")
    DBBackend().import_dataset(datasetname, filename)


def add_version(osm_filename):
    tree = ElementTree.parse(osm_filename)
    for element in tree.getroot():
        if (
            element.tag in ["node", "way", "relation"]
            and "version" not in element.attrib
        ):
            element.set("version", "0")
    tree.write(osm_filename + ".new")
    shutil.move(osm_filename + ".new", osm_filename)


def add_boundingbox(osm_filename, bounds):
    tree = ElementTree.parse(osm_filename)
    ET.SubElement(
        tree.getroot(),
        "bounds",
        attrib={
            "minlat": bounds.lat1,
            "minlon": bounds.lon1,
            "maxlat": bounds.lat2,
            "maxlon": bounds.lon2,
            "origin": "serve_osm",
        },
    )
    tree.write(osm_filename + ".new")
    shutil.move(osm_filename + ".new", osm_filename)


if __name__ == "__main__":
    app.run()
