#!/usr/bin/env python3
import logging
import os
import xml.etree.ElementTree as ElementTree

import psycopg
from psycopg import Connection
from psycopg.types.json import Json
from tqdm import tqdm

from ..osm import BBox, DataSet, LatLon, Node


class DBBackend(object):
    __NODE = "node"
    DATABASE = os.getenv("POSTGRES_DB", "serveosm")
    POSTGRES_ENDPOINT = os.getenv("POSTGRES_ENDPOINT", "127.0.0.1")
    POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD", None)
    POSTGRES_USER = os.getenv("POSTGRES_USER", "serveosm")
    POSTGRES_PORT = os.getenv("POSTGRES_PORT", "5432")

    def __init__(
        self,
        database: str = DATABASE,
        endpoint: str = POSTGRES_ENDPOINT,
        password: str = POSTGRES_PASSWORD,
        user: str = POSTGRES_USER,
        port: str = POSTGRES_PORT,
    ):
        self.database = database
        self.endpoint = endpoint
        self.password = password
        self.user = user
        self.port = port

    def get_connection(self, autocommit=False) -> Connection:
        """Get the connection to use"""
        return psycopg.connect(
            dbname=self.database,
            user=self.user,
            password=self.password,
            host=self.endpoint,
            port=self.port,
            autocommit=autocommit,
        )

    def _create_schema(self, connection: Connection, name: str) -> None:
        """Create the schema for the db"""
        try:
            cursor = connection.cursor()
            cursor.execute(
                """
                CREATE TABLE IF NOT EXISTS import_data_date
                (id SERIAL PRIMARY KEY, name text, date date)
                """
            )  # TODO use for versioning
            cursor.execute("CREATE SCHEMA IF NOT EXISTS {}".format(name))
            cursor.execute(
                "DROP TABLE IF EXISTS {}.{}_new".format(name, DBBackend.__NODE)
            )
            cursor.execute(
                """
                CREATE TABLE {}.{}_new
                (id SERIAL PRIMARY KEY, version INTEGER NOT NULL, coordinates
                GEOMETRY(POINT, 4326), tags jsonb)
                """.format(
                    name, DBBackend.__NODE
                )
            )
            connection.commit()
        except Exception as exception:
            connection.rollback()
            raise exception

    def _load_data(
        self, connection: Connection, name: str, dataset: str
    ) -> None:
        try:
            cursor = connection.cursor()
            iterabletree = ElementTree.iterparse(
                dataset, events=("start", "end")
            )
            root = None
            for event, element in tqdm(iterabletree, mininterval=30):
                if event == "end" and element.tag in [
                    "node",
                    "way",
                    "relation",
                ]:
                    primitive = DataSet.element_to_osm_object(element)
                    element.clear()
                    if root is not None:
                        root.clear()
                    if primitive and isinstance(primitive, Node):
                        cursor.execute(
                            """
                            INSERT INTO {}.{}_new
                            (id, version, coordinates, tags)
                            VALUES ({}, {}, ST_GeomFromEWKT('{}'), %s)
                            """.format(
                                name,
                                DBBackend.__NODE,
                                primitive.id,
                                primitive.version,
                                primitive.latlon.to_wkt(),
                            ),
                            [
                                Json(primitive.json_tags()),
                            ]
                        )
                    elif primitive:
                        raise ValueError(
                            "We don't currently support {} for imports".format(
                                type(primitive)
                            )
                        )
                elif event == "start" and element.tag == "osm":
                    root = element
            connection.commit()
        except Exception as exception:
            connection.rollback()
            raise exception

    def _rename_tables(self, connection: Connection, name: str) -> None:
        """Rename tables"""
        try:
            cursor = connection.cursor()
            logging.error("Renaming tables")
            cursor.execute(
                "DROP TABLE IF EXISTS {}.{}_old".format(name, DBBackend.__NODE)
            )
            cursor.execute(
                "ALTER TABLE IF EXISTS {0}.{1} RENAME TO {1}_old".format(
                    name, DBBackend.__NODE
                )
            )  # TODO versioning
            cursor.execute(
                "ALTER TABLE {0}.{1}_new RENAME TO {1}".format(
                    name, DBBackend.__NODE
                )
            )
            connection.commit()
        except Exception as exception:
            connection.rollback()
            raise exception

    def _drop_old_tables(self, connection: Connection, name: str):
        try:
            cursor = connection.cursor()
            cursor.execute(
                "DROP INDEX IF EXISTS {0}.{1}_idx".format(
                    name, DBBackend.__NODE
                )
            )
            cursor.execute(
                "DROP INDEX IF EXISTS {0}.{1}_old_idx".format(
                    name, DBBackend.__NODE
                )
            )
            cursor.execute(
                (
                    "CREATE INDEX {1}_idx ON {0}.{1} "
                    + "USING GIST(coordinates)"
                ).format(name, DBBackend.__NODE)
            )
            cursor.execute(
                (
                    "CREATE INDEX {1}_old_idx ON {0}.{1}_old "
                    + "USING GIST(coordinates)"
                ).format(name, DBBackend.__NODE)
            )
            connection.commit()
        except Exception as exception:
            connection.rollback()
            raise exception

    def _optimize_new_table(self, name: str):
        with self.get_connection(autocommit=True) as connection:
            cursor = connection.cursor()
            cursor.execute(
                "VACUUM ANALYZE {}.{}_new".format(name, DBBackend.__NODE)
            )
            connection.commit()

    def import_dataset(self, name: str, dataset: str):
        """Import a dataset"""
        logging.error("Starting import of {} {}".format(name, dataset))
        with self.get_connection() as connection:
            self._create_schema(connection, name)
            self._load_data(connection, name, dataset)
            self._optimize_new_table(name)
            self._rename_tables(connection, name)
            self._drop_old_tables(connection, name)
            logging.error("Finished import")

    def get_data(self, dataset: str, bbox: BBox):
        cursor = self.get_connection().cursor()
        elements = []
        query = (
            "SELECT id, version, ST_AsText(coordinates), tags FROM {}.{} "
            + "WHERE coordinates && "
            + "ST_MakeEnvelope({xmin}, {ymin}, {xmax}, {ymax}, 4326)"
        ).format(
            dataset,
            DBBackend.__NODE,
            xmin=bbox.lon1,
            ymin=bbox.lat1,
            xmax=bbox.lon2,
            ymax=bbox.lat2,
        )
        cursor.execute(query)
        for nodeid, version, coordinates, tags in cursor:
            coordinates = (
                coordinates.replace("POINT(", "").replace(")", "").split(" ")
            )
            latlon = LatLon(coordinates[1], coordinates[0])
            elements.append(Node(nodeid, latlon, tags, version=version))
        return DataSet(bbox, elements)
