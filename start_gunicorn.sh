#!/usr/bin/env sh

gunicorn --log-level debug --chdir "$(pwd)" serve_osm_files:app --workers 2 --threads 2 --bind 0.0.0.0:8000
