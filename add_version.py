#!/usr/bin/env python3
import shutil
import sys
import xml.etree.ElementTree as ElementTree


def add_version(osm_filename):
    tree = ElementTree.parse(osm_filename)
    for element in tree.getroot():
        if (
            element.tag in ["node", "way", "relation"]
            and "version" not in element.attrib
        ):
            element.set("version", "0")
    tree.write(osm_filename + ".new")
    shutil.move(osm_filename + ".new", osm_filename)


if __name__ == "__main__":
    add_version(sys.argv[1])
