FROM python:latest
WORKDIR /app
EXPOSE 8000
RUN apt-get update && apt-get install -y libgeos-dev libgdal-dev libboost-numpy-dev && rm -rf /var/lib/apt/lists/*
RUN pip3 install gunicorn gdal==$(gdal-config --version) psycopg[c]
COPY requirements.txt /app/
RUN pip3 install -r /app/requirements.txt

COPY serve_osm_files/ /app/serve_osm_files/
COPY *.py requirements.txt serve_osm.wsgi start_gunicorn.sh updateFiles.sh LICENSE /app/

CMD ["./start_gunicorn.sh"]

